import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import logger from "redux-logger";
import shoppingReducer from "./reducers/shopping-reducer";
import dataReducer from "./reducers/data-reducer";
import dataUserReducer from "./reducers/data-user-reducer";
import dataPostReducer from "./reducers/data-post-reducer";

const reducers = combineReducers({
  shoppingList: shoppingReducer
  // dataList: dataReducer,
  // dataUserList: dataUserReducer,
  // dataPost: dataPostReducer
});

const middlewares = applyMiddleware(thunk, logger);
const composeEnhancers = composeWithDevTools(middlewares);

const store = createStore(reducers, composeEnhancers);

export default store;
