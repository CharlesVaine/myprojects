import {
  FETCHING_DATA_USERS,
  FETCHING_DATA_USERS_SUCCESS,
  FETCHING_DATA_USERS_FAILURE
} from "../actions/Fetch-users";

const initialState = {
  data: [],
  isFetching: false,
  error: false
};

function dataUserReducer(prevState = initialState, action) {
  switch (action.type) {
    case FETCHING_DATA_USERS:
      return {
        ...prevState,
        data: [],
        isFetching: true
      };
    case FETCHING_DATA_USERS_SUCCESS:
      return {
        ...prevState,
        data: action.data,
        isFetching: false
      };
    case FETCHING_DATA_USERS_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return {
        prevState
      };
  }
}

export default dataUserReducer;
