import {
  FETCHING_DATA_POST,
  FETCHING_DATA_POST_SUCCESS,
  FETCHING_DATA_POST_FAILURE
} from "../actions/Fetch-action-post";

const initialState = {
  data: [],
  isFetching: false,
  error: false
};

function dataPostReducer(prevState = initialState, action) {
  switch (action.type) {
    case FETCHING_DATA_POST:
      return {
        ...prevState,
        data: [],
        isFetching: true
      };
    case FETCHING_DATA_POST_SUCCESS:
      return {
        ...prevState,
        data: [...prevState.data, action.payload],
        isFetching: false
      };
    case FETCHING_DATA_POST_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return {
        prevState
      };
  }
}

export default dataPostReducer;
