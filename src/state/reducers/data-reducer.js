import {
  FETCHING_DATA,
  FETCHING_DATA_SUCCESS,
  FETCHING_DATA_FAILURE
} from "../actions/Fetch-action";

const initialState = {
  data: [],
  isFetching: false,
  error: false
};

function dataReducer(prevState = initialState, action) {
  switch (action.type) {
    case FETCHING_DATA:
      return {
        ...prevState,
        data: [],
        isFetching: true
      };
    case FETCHING_DATA_SUCCESS:
      return {
        ...prevState,
        data: action.data,
        isFetching: false
      };
    case FETCHING_DATA_FAILURE:
      return {
        ...prevState,
        isFetching: false,
        error: true
      };
    default:
      return {
        prevState
      };
  }
}

export default dataReducer;
