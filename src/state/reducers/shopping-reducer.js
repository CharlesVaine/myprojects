import {
  ADD_ITEM,
  REMOVE_ITEM,
  ADD_ITEM_SUCCESS,
  ADD_ITEM_FAIL
} from "../actions/ShoppingList-actions";

const initialState = {
  items: [],
  isFetching: false,
  error: false
};

function shoppingReducer(prevState = initialState, action) {
  switch (action.type) {
    case ADD_ITEM:
      return {
        ...prevState,
        isFetching: true
      };
    case ADD_ITEM_SUCCESS:
      return {
        ...prevState,
        isFetching: false,
        items: [...prevState.items, action.payload]
      };
    case ADD_ITEM_FAIL:
      return {
        ...prevState,
        error: true
      };
    case REMOVE_ITEM:
      let afterDelete = prevState.items.filter(item => {
        return item.id !== action.payload.id;
      });
      return {
        ...prevState,
        isFetching: false,
        items: afterDelete
      };
    default:
      return prevState;
  }
}

export default shoppingReducer;
