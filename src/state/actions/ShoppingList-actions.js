// ACTIONS
export const ADD_ITEM = "addItem";
export const actionAdd = () => ({
  type: ADD_ITEM
});

export const ADD_ITEM_SUCCESS = "addItemSucess";
export const actionAddSuccess = ({ id, name }) => ({
  type: ADD_ITEM_SUCCESS,
  payload: {
    id,
    name
  }
});

export const ADD_ITEM_FAIL = "addItemSucess";
export const actionAddFail = () => ({
  type: ADD_ITEM_FAIL
});

export const REMOVE_ITEM = "removeItem";
export const actionRemove = id => ({
  type: REMOVE_ITEM,
  payload: {
    id
  }
});

// THUNK

// export function startAsyncAction(id, name) {
//   return function(dispatch) {
//     dispatch(actionAdd());
//     setTimeout(() => {
//       dispatch(actionAddSuccess(id, name));
//     }, 3000);
//   };
// }

export const startAsyncAction = (id, name) => {
  return dispatch => {
    dispatch(actionAdd());

    return fetch("https://jsonplaceholder.typicode.com/todos", {
      method: "POST", // or 'PUT'
      body: JSON.stringify({ id, name }), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(() => {
        dispatch(actionAddSuccess({ id, name }));
      })

      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(actionAddFail());
      });
  };
};
