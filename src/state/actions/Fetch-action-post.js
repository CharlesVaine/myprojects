export const FETCHING_DATA_POST = "FETCHING_DATA_POST";
export const postData = () => {
  return {
    type: FETCHING_DATA_POST
  };
};

export const FETCHING_DATA_POST_SUCCESS = "FETCHING_DATA_POST_SUCCESS";
export const postDataSuccess = (id, name) => {
  return {
    type: FETCHING_DATA_POST_SUCCESS,
    payload: {
      id,
      name
    }
  };
};

export const FETCHING_DATA_POST_FAILURE = "FETCHING_DATA_POST_FAILURE";
export const postDataFailure = error => {
  return {
    type: FETCHING_DATA_POST_FAILURE,
    error: error
  };
};

// EJEMPLO DE CÓMO SERÍA UN POST con THUNK
export const fetchDataPost = ({ id, name }) => {
  // return function
  return dispatch => {
    dispatch(postData());
    return fetch("https://jsonplaceholder.typicode.com/todos", {
      method: "POST", // or 'PUT'
      body: { id, name }, // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(json => {
        dispatch(postDataSuccess(json));
      })
      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(postDataFailure());
      });
  };
};
