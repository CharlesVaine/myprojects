export const FETCHING_DATA_USERS = "FETCHING_DATA_USERS";
export const getDataUsers = () => {
  return {
    type: FETCHING_DATA_USERS
  };
};

export const FETCHING_DATA_USERS_SUCCESS = "FETCHING_DATA_USERS_SUCCESS";
export const getDataUsersSuccess = data => {
  return {
    type: FETCHING_DATA_USERS_SUCCESS,
    data
  };
};

export const FETCHING_DATA_USERS_FAILURE = "FETCHING_DATA_USERS_FAILURE";
export const getDataUsersFailure = () => {
  return {
    type: FETCHING_DATA_USERS_FAILURE
  };
};

// THUNK
// export const fetchDataUsers = () => {
//   // return function
//   return dispatch => {
//     dispatch(getDataUsers());
//     return fetch("https://api.tvmaze.com/seasons/6/episodes")
//       .then(response => Promise.all([response, response.json()]))
//       .then(([response, json]) => {
//         dispatch(getDataUsersSuccess(json));
//       })
//       .catch(err => console.log(err));
//   };
// };

// THUNK

export const fetchDataUsers = () => {
  // return function
  return dispatch => {
    dispatch(getDataUsers());
    return fetch(`https://api.tvmaze.com/seasons/6/episodes`)
      .then(
        response => response.json(),
        error => console.log("An error occurred", error)
      )
      .then(json => {
        dispatch(getDataUsersSuccess(json));
      })
      .catch(function(error) {
        console.log("Hubo un problema con la petición Fetch:" + error.message);
        dispatch(getDataUsersFailure());
      });
  };
};
