import getDataApi from "../api";

export const FETCHING_DATA = "FETCHING_DATA";
export const getData = () => {
  return {
    type: FETCHING_DATA
  };
};

export const FETCHING_DATA_SUCCESS = "FETCHING_DATA_SUCCESS";
export const getDataSuccess = data => {
  return {
    type: FETCHING_DATA_SUCCESS,
    data
  };
};

export const FETCHING_DATA_FAILURE = "FETCHING_DATA_FAILURE";
export const getDataFailure = () => {
  return {
    type: FETCHING_DATA_FAILURE
  };
};

// THUNK
export const fetchData = () => {
  // return function
  return dispatch => {
    dispatch(getData());
    getDataApi()
      .then(([response, json]) => {
        dispatch(getDataSuccess(json));
      })
      .catch(err => console.log(err));
  };
};
