const URL = "http://api.tvmaze.com/search/shows?q=girls";

export default () => {
  return fetch(URL).then(response => Promise.all([response, response.json()]));
};
