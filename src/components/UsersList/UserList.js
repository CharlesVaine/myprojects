import React from "react";
import { connect } from "react-redux";
import { fetchDataUsers } from "../../state/actions/Fetch-users";

class UserList extends React.Component {
  /*
  componentDidMount() {
    this.props.fetchDataUserApi();
  }*/

  render() {
    return (
      <div>
        <ul>
          {this.props.isFetching && <li>Cargando</li>}
          {this.props.dataUsers &&
            this.props.dataUsers.map((user, index) => (
              <li key={index}>{user.name}</li>
            ))}
        </ul>
        <button onClick={() => this.props.fetchDataUserApi()} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dataUsers: state.dataUserList.data,
  isFetching: state.dataUserList.isFetching,
  error: state.dataUserList.error
});

const mapDispatchToProps = dispatch => {
  return {
    fetchDataUserApi: () => dispatch(fetchDataUsers())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
