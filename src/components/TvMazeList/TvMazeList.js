import React from "react";
import { connect } from "react-redux";
import { fetchData } from "../../state/actions/Fetch-action";

class TvMaze extends React.Component {
  componentDidMount() {
    this.props.fetchDataApi();
  }

  render() {
    return (
      <div>
        <ul>
          {this.props.dataTvMaze &&
            this.props.dataTvMaze.map((TvMazeElement, index) => (
              <li key={index}>{TvMazeElement.show.name}</li>
            ))}
        </ul>
        {/* <button onClick={() => this.fetchData()} /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dataTvMaze: state.dataList.data
});

const mapDispatchToProps = dispatch => {
  return {
    fetchDataApi: () => dispatch(fetchData())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TvMaze);
