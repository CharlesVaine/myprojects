import React from "react";
import { connect } from "react-redux";
import {
  actionRemove,
  startAsyncAction
} from "../../state/actions/ShoppingList-actions";
import FlipMove from "react-flip-move";
import "./ShoppingList.css";

class Shopping extends React.Component {
  state = {
    name: ""
  };

  updateName(value) {
    this.setState({ name: value });
  }

  render() {
    return (
      <div className="ShoppingListMain">
        <div className="header">
          <input
            id="inputShopping"
            onKeyUp={e => this.updateName(e.target.value)}
          />
          <button
            onClick={() => {
              this.state.name !== "" &&
                this.props.onAddItem(
                  Math.floor(Math.random() * 99) + 1,
                  this.state.name
                );
            }}
          >
            Add
          </button>
          {!this.props.isFetching && <p style={{ color: "gold" }} />}
          {this.props.isFetching && (
            <p style={{ color: "gold" }}>Fetching new item...</p>
          )}
        </div>
        <div>
          <ul className="theList">
            <FlipMove duration={250} easing="ease-out">
              {this.props.ShoppingList.map((shoppingElement, index) => (
                <li
                  id={shoppingElement.id}
                  key={index}
                  onClick={() => {
                    this.props.onRemoveItem(shoppingElement.id);
                  }}
                >
                  {shoppingElement.name}
                </li>
              ))}
            </FlipMove>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ShoppingList: state.shoppingList.items,
  isFetching: state.shoppingList.isFetching
});

const mapDispatchToProps = dispatch => {
  return {
    onAddItem: (id, name) => {
      dispatch(startAsyncAction(id, name));
    },
    onRemoveItem: id => {
      dispatch(actionRemove(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shopping);
