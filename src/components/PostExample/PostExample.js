import React from "react";
import { connect } from "react-redux";
import FlipMove from "react-flip-move";
import { fetchDataPost } from "../../state/actions/Fetch-action-post";

class PostExample extends React.Component {
  state = {
    name: ""
  };

  updateName(value) {
    this.setState({ name: value });
  }

  render() {
    return (
      <div className="ShoppingListMain">
        <div className="header">
          <input
            id="inputShopping"
            onKeyUp={e => this.updateName(e.target.value)}
          />
          <button
            onClick={() => {
              this.state.name !== "" &&
                this.props.postData(Math.random(), this.state.name);
            }}
          >
            Add
          </button>
        </div>
        <div>
          <ul className="theList">
            <FlipMove duration={250} easing="ease-out">
              {this.props.isFetching === false &&
                (this.props.postItems.length > 0 &&
                  this.props.postItems.map((postItem, index) => (
                    <li id={postItem.id} key={index}>
                      {postItem.name}
                    </li>
                  )))}
            </FlipMove>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  postItems: state.dataPost.data,
  isFetching: state.dataPost.isFetching
});

const mapDispatchToProps = dispatch => {
  return {
    postData: (id, name) => {
      dispatch(fetchDataPost(id, name));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostExample);
