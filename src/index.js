import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import ShoppingList from "./components/ShoppingList/";
//import TvMazeList from "./components/TvMazeList/TvMazeList";
import { Provider } from "react-redux";
import store from "./state/store";
//import UserList from "./components/UsersList/UserList";
// import PostExample from "./components/PostExample";

const App = () => (
  <Provider store={store}>
    <ShoppingList />
    {/* <TvMazeList />
    <UserList /> */}
  </Provider>
);
ReactDOM.render(<App />, document.getElementById("container"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
